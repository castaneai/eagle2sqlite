DROP TABLE IF EXISTS folders;
CREATE TABLE folders (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL DEFAULT ''
);

DROP TABLE IF EXISTS items;
CREATE TABLE items (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    size INT NOT NULL,
    ext TEXT NOT NULL,
    width INT NOT NULL,
    height INT NOT NULL,
    thumbnail_url TEXT NOT NULL DEFAULT '',
    content_url TEXT NOT NULL DEFAULT '',
    source_url TEXT NOT NULL DEFAULT ''
);

DROP TABLE IF EXISTS tags;
CREATE TABLE tags (
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL COLLATE NOCASE
);
CREATE INDEX tags_name ON tags(name);

DROP TABLE IF EXISTS folders_items;
CREATE TABLE folders_items (
    folder_id TEXT NOT NULL,
    item_id TEXT NOT NULL
);
CREATE UNIQUE INDEX folders_items_unique ON folders_items(folder_id, item_id);

DROP TABLE IF EXISTS items_tags;
CREATE TABLE items_tags (
    item_id TEXT NOT NULL,
    tag_id TEXT NOT NULL
);
CREATE UNIQUE INDEX items_tags_unique ON items_tags(item_id, tag_id);
CREATE INDEX items_tags_id ON items_tags(tag_id);
