import requests
import sqlite3

_EAGLE_API = "http://localhost:41595"


def recreate():
    with sqlite3.connect("eagle.sqlite") as conn:
        run_recreate(conn)


def update():
    with sqlite3.connect("eagle.sqlite") as conn:
        run_update(conn)


def run_recreate(conn: sqlite3.Connection):
    with open("init.sql") as f:
        sql = f.read()
        conn.executescript(sql)


def run_update(conn: sqlite3.Connection):
    resp = requests.get(_EAGLE_API + "/api/folder/list").json()
    for folder in resp["data"]:
        conn.execute("INSERT OR IGNORE INTO folders VALUES (?, ?, ?)", (
            folder["id"],
            folder["name"],
            folder["description"],
        ))

    resp = requests.get(_EAGLE_API + "/api/item/list?limit=999999").json()
    items = filter(lambda i: not i["isDeleted"], resp["data"])
    items_updated = 0
    for item in items:
        cur = conn.cursor()
        try:
            for folder_id in item["folders"]:
                cur.execute("INSERT OR IGNORE INTO folders_items VALUES(?, ?)", (
                    folder_id,
                    item["id"],
                ))
            cur.execute("INSERT OR IGNORE INTO items VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", (
                item["id"],
                item["name"],
                item["size"],
                item["ext"],
                item["width"],
                item["height"],
                "",
                "",
                item["url"],
            ))
            items_updated += cur.rowcount
            conn.commit()
        except:
            print(f"failed to insert: {item}")
            conn.rollback()
    print(f"{items_updated} items has been updated.")
