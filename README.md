# eagle2sqlite

## Usage

```sh
# recreate database
poetry run recreate

# update database
poetry run update
```

## LICENSE

MIT
